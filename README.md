# SilverStripe Mobiledetect #

### Overview ###

For some websites it's desirable to make it adaptive instead of responsive. This module detects if the users device is a mobile phone or not. If it ís a mobile phone the user will be redirected to the separate mobile theme. You can choose: redirected the mobile user to a separate (sub)domain with the mobile theme or just display the mobile theme at the same main domain.

Based on the SilverStripe Mobile module (https://github.com/silverstripe/silverstripe-mobile). But with this Adjustments:

* Mobile detection is handled by the PHP class Mobile Detect (http://mobiledetect.net/). This class is regularly updated with the newest mobile devices.
* This module sees only mobile phones as mobile device, tablets are not treated as mobile device because in most situations the desktop version is displayed well on tablets.

### Version ###

Using Semantic Versioning.

### Requirements ###

* SilverStripe > 3.1, tested untill 3.3.1.

### Installation ###

composer require hestec/silverstripe-mobiledetect 1.*

### Usage ###

In the CMS, browse to the "Settings" tab on the left navigation bar to access the SiteConfig settings.
