<?php
/**
 * This class uses the Mobile Detect class:
 * http://mobiledetect.net/
 * https://github.com/serbanghita/Mobile-Detect/
 * This class is added to /vendor if you installed this module by composer.
 */
class MobileBrowserDetector {

	public static function is_mobile() {

		$detect = new Mobile_Detect;
		// Exclude tablets.
		if( $detect->isMobile() && !$detect->isTablet() ){
			return true;
		}else{
			return false;
		}

	}
}
